# Changes

## 0.0.0-3

- introduce concept of handlers
  - pathHandler /:path
  - rootHandler /
  - userHomeHandler ~/
- functionality is broken as /:path is not absolutely dynamic; and express cannot serve /home/f0c1s/.ssh because there is no handler for it.
