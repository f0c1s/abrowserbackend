"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var userHomeHandler_1 = __importDefault(require("./handlers/userHomeHandler"));
var rootHandler_1 = __importDefault(require("./handlers/rootHandler"));
var userinfo_1 = __importDefault(require("./userinfo"));
var pathHandler_1 = __importDefault(require("./handlers/pathHandler"));
var express = require('express');
var app = express();
var port = process.env.A_BROWSER_SERVER_PORT || 6001;
app.locals.userinfo = userinfo_1.default();
app.get('/', function (_, res) {
    res.json(rootHandler_1.default());
});
app.get("" + app.locals.userinfo.homedir, function (_, res) {
    console.log('fetching home...');
    res.json(userHomeHandler_1.default());
});
app.get("/:path", function (req, res) {
    var path = req.params['path'] || app.locals.userinfo.homedir;
    console.log("path received: " + path);
    res.json(pathHandler_1.default("/" + path));
});
app.listen(port, function () {
    console.log("Listening at http://localhost:" + port);
});
//# sourceMappingURL=server.js.map