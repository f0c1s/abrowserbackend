/// <reference types="node" />
declare function userHomeHandler(): {
    '~': any;
    user: import("os").UserInfo<string>;
};
export default userHomeHandler;
//# sourceMappingURL=userHomeHandler.d.ts.map