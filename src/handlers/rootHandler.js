"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var userinfo_1 = __importDefault(require("../userinfo"));
var directories = require('node-directories');
function rootHandler() {
    var dirs = directories('/');
    return { user: userinfo_1.default(), '/': dirs };
}
exports.default = rootHandler;
//# sourceMappingURL=rootHandler.js.map