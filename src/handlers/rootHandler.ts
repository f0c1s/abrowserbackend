import userinfo from "../userinfo";

const directories = require('node-directories');

function rootHandler() {
    const dirs = directories('/');
    return {user: userinfo(), '/': dirs};
}

export default rootHandler;
