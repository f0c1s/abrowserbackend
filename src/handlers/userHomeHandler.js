"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var userinfo_1 = __importDefault(require("../userinfo"));
var directories = require('node-directories');
function userHomeHandler() {
    var userInfo = userinfo_1.default();
    var homedir = userInfo.homedir;
    var dirs = directories(homedir);
    return { '~': dirs, user: userInfo };
}
exports.default = userHomeHandler;
//# sourceMappingURL=userHomeHandler.js.map