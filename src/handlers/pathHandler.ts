import * as fs from "fs";

const directories = require('node-directories');

function pathHandler(path: string) {
    return fs.existsSync(path) ? directories(path) : [];
}

export default pathHandler;
