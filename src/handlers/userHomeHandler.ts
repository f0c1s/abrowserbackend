import userinfo from "../userinfo";

const directories = require('node-directories');

function userHomeHandler() {
    const userInfo = userinfo();
    const {homedir} = userInfo;
    const dirs = directories(homedir)

    return {'~': dirs, user: userInfo};
}

export default userHomeHandler;
