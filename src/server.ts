import {Request, Response} from "express";
import userHomeHandler from "./handlers/userHomeHandler";
import rootHandler from "./handlers/rootHandler";
import userinfo from "./userinfo";
import pathHandler from "./handlers/pathHandler";

const express = require('express');
const app = express();
const port = process.env.A_BROWSER_SERVER_PORT || 6001;

app.locals.userinfo = userinfo();

app.get('/', (_: any, res: any) => {
    res.json(rootHandler());
});

app.get(`${app.locals.userinfo.homedir}`, (_: any, res: Response) => {
    console.log('fetching home...')
    res.json(userHomeHandler());
});

app.get(`/:path`, (req: Request, res: Response) => {
    const path = req.params['path'] || app.locals.userinfo.homedir;
    console.log(`path received: ${path}`);
    res.json(pathHandler(`/${path}`));
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});