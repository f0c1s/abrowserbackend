import * as os from "os";

function userinfo() {
    return os.userInfo();
}

export default userinfo;
