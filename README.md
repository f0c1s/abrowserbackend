# aBrowser #

A browser application for your computer. Backend application.

## Server Application

* It is written in nodejs.
* It uses typescript.
* Most of the functionality is tested via Jasmine.

## How do I get set up?

* clone
* `npm i`
* Database configuration: we plan to use postgres
* How to run tests: `npm run test`
* Deployment instructions: `npm run start`

## Contribution guidelines

* Writing tests: very important; submit code and tests together.
* Code review: code should be readable, maintainable, and secure
* Other guidelines: remember that you are working with other humans. Be decent.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact: when we have one.
